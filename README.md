# LeetCode2Tasks

easy:
66. Plus One https://leetcode.com/problems/plus-one/?envType=study-plan-v2&envId=top-interview-150

69. Sqrt(x) https://leetcode.com/problems/sqrtx/

70. Climbing Stairs https://leetcode.com/problems/climbing-stairs/

118. Pascal's Triangle https://leetcode.com/problems/pascals-triangle/description/

121. Best Time to Buy and Sell Stock https://leetcode.com/problems/best-time-to-buy-and-sell-stock/

136. Single Number https://leetcode.com/problems/single-number/

169. Majority Element https://leetcode.com/problems/majority-element/description/

231. Power of Two https://leetcode.com/problems/power-of-two/

268. Missing Number https://leetcode.com/problems/missing-number/description/

283. Move Zeroes https://leetcode.com/problems/move-zeroes/

383. Ransom Note https://leetcode.com/problems/ransom-note/

389. Find the Difference https://leetcode.com/problems/find-the-difference/

409. Longest Palindrome https://leetcode.com/problems/longest-palindrome/

500. Keyboard Row https://leetcode.com/problems/keyboard-row/

599. Minimum Index Sum of Two Lists https://leetcode.com/problems/minimum-index-sum-of-two-lists/

697. Degree of an Array https://leetcode.com/problems/degree-of-an-array/

704. Binary Search https://leetcode.com/problems/binary-search/

876. Middle of the Linked List https://leetcode.com/problems/middle-of-the-linked-list/

1266. Minimum Time Visiting All Points https://leetcode.com/problems/minimum-time-visiting-all-points/

1331. Rank Transform of an Array https://leetcode.com/problems/rank-transform-of-an-array/

1480. Running Sum of 1d Array https://leetcode.com/problems/running-sum-of-1d-array/

1732. Find the Highest Altitude https://leetcode.com/problems/find-the-highest-altitude/?envType=study-plan-v2&envId=leetcode-75

1903. Largest Odd Number in String https://leetcode.com/problems/largest-odd-number-in-string/

1925. Count Square Sum Triples https://leetcode.com/problems/count-square-sum-triples/

1935. Maximum Number of Words You Can Type https://leetcode.com/problems/maximum-number-of-words-you-can-type/description/

1952. Three Divisors https://leetcode.com/problems/three-divisors/

2160. Minimum Sum of Four Digit Number After Splitting Digits https://leetcode.com/problems/minimum-sum-of-four-digit-number-after-splitting-digits/

2729. Check if The Number is Fascinating https://leetcode.com/problems/check-if-the-number-is-fascinating/

medium:

75. Sort Colors https://leetcode.com/problems/sort-colors/

128. Longest Consecutive Sequence https://leetcode.com/problems/longest-consecutive-sequence/

148. Sort List https://leetcode.com/problems/sort-list/description/

2177. Find Three Consecutive Integers That Sum to a Given Number https://leetcode.com/problems/find-three-consecutive-integers-that-sum-to-a-given-number/


