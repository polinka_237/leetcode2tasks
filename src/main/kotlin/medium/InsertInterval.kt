package medium

//https://leetcode.com/problems/insert-interval/?envType=study-plan-v2&envId=top-interview-150
class InsertInterval {
    class Solution {
        fun insert(intervals: Array<IntArray>, newInterval: IntArray): Array<IntArray> {
            if (intervals.size == 0) {
                return arrayOf(newInterval)
            }

            val (start, end) = newInterval
            val ans = mutableListOf<IntArray>()
            val needMerge = mutableSetOf<Int>()

            for (i in 0 until intervals.size) {
                val (a, b) = intervals[i]
                if (start in a..b || end in a..b || (start < a && end > b)) {
                    needMerge.add(i)
                }
            }

            var min = newInterval[0]
            var max = newInterval[1]

            for (i in needMerge) {
                val (a, b) = intervals[i]
                min = Math.min(min, a)
                max = Math.max(max, b)
            }

            var isReplaced = false
            for (i in 0 until intervals.size) {
                if (needMerge.size > 0) {
                    if (i in needMerge) {
                        if (isReplaced) {}
                        else {
                            isReplaced = true
                            ans.add(intArrayOf(min, max))
                        }
                    } else {
                        ans.add(intervals[i])
                    }
                } else {
                    if (!isReplaced) {
                        val (a, b) = newInterval
                        if (b < intervals[i][0]) {
                            ans.add(newInterval)
                            isReplaced = true
                        }
                    }
                    ans.add(intervals[i])
                }

            }

            if (!isReplaced) ans.add(newInterval)

            return ans.toTypedArray()
        }
    }

    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            val intervals = arrayOf(intArrayOf(1,3), intArrayOf(6,9))
            val nums = intArrayOf(2,5)
            println("intervals = ${intervals.contentToString()}")

            for (i in intervals.indices) {

            }
            println("nums = ${nums.contentToString()}")
            val res = Solution().insert(intervals, nums)
            println("res = ${res.contentToString()}")
        }
    }
}