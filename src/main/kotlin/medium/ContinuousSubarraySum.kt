package medium

//https://leetcode.com/problems/continuous-subarray-sum/

class ContinuousSubarraySum {
    class Solution {
        fun checkSubarraySum(nums: IntArray, k: Int): Boolean {
//            val map = mutableMapOf<Int, Int>()
            var sum = 0
            if (nums.isEmpty()) return false
            for (i in 0 until nums.size-1) {
               sum = nums[i]
                for (j in i+1 until nums.size) {
                    sum += nums[j]
                    if (k != 0 && sum % k == 0) {
                        return true
                    }
                }
            }
            return false
        }
    }
    companion object {
        @JvmStatic
        fun main(agrs: Array<String>) {
            val nums = intArrayOf(23,2,4,6,7)
            println("nums = ${nums.contentToString()}")
            val res = Solution().checkSubarraySum(nums,6)
            println("res = $res")
        }
    }
}