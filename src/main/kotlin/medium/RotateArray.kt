package medium

class RotateArray {
    class Solution {
        fun rotate(nums: IntArray, k: Int): Unit {
            for (i in 0 until k) {
                val cnt = nums[nums.size-1]
                for (j in nums.size - 2 downTo 0) {
                    nums[j+1] = nums[j]
                }
                nums[0] = cnt
            }
        }
    }
    companion object {
        @JvmStatic
        fun main(agrs: Array<String>) {
            val nums = intArrayOf(1,2,3,4,5,6,7)
            println("nums = ${nums.contentToString()}")
            val res = Solution().rotate(nums, 3)
            println("res = ${nums.contentToString()}")
        }
    }
}