package medium

class FindThreeConsecutiveIntegersThatSumtoAGivenNumber {
    class Solution {
        fun sumOfThree(num: Long): LongArray {

//            val result = ArrayList<Long>()
//            val n = (num-3)
//            var i = 0L
//            while(i < n) {
//                val x = i
//                val y = (i+1)
//                val z = (i+2)
//                i++
//                if (x+y+z == num) {
//                    result.add(x)
//                    result.add(y)
//                    result.add(z)
//
//                } else result
//
//            }
//            for(i in 1 until num-3) {
//                val x = i.toLong()
//                val y = (i+1).toLong()
//                val z = (i+2).toLong()
//                if (x+y+z == num) {
//                    result.add(x)
//                    result.add(y)
//                    result.add(z)
//                } else result
//            }
            //return result.toLongArray()


            val res = LongArray(3)
            val res2 = LongArray(0)

            val cnt = num / 3

            return if (cnt - 1 + cnt + (cnt + 1) == num) {
                res[0] = cnt - 1
                res[1] = cnt
                res[2] = cnt + 1
                res
            } else res2
        }
    }
    companion object {
        @JvmStatic
        fun main(agrs: Array<String>) {
            val num = 562789250L
            println("num = $num")
            val res = Solution().sumOfThree(num)
            println("res = ${res.contentToString()}")
        }
    }
}