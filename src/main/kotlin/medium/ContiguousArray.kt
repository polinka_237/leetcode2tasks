package medium


//https://leetcode.com/problems/contiguous-array/
class ContiguousArray {
    class Solution {
        fun findMaxLength(nums: IntArray): Int {
            var map = HashMap<Int, Int>()
            var cnt = 0
            var maxlen = 0
            for (i in nums.indices) {
                if (nums[i] == 0) {
                    cnt += nums[i]
                } else cnt = cnt + (-1)
                if (cnt == 0) {
                    maxlen = maxlen+1
                } else if (map.containsKey(cnt)) {
                    maxlen = Math.max(maxlen, i-map[cnt]!!)
                } else map[cnt] = i


            }
            return maxlen


        }
    }
    companion object {
        @JvmStatic
        fun main(agrs: Array<String>) {
            val nums = intArrayOf(0,1,0)

            println("nums = ${nums.contentToString()}")

            val res = Solution().findMaxLength(nums)
            println("res = $res")
        }
    }
}