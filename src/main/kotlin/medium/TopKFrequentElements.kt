package medium

import java.util.*


class TopKFrequentElements {
    class Solution {
        fun topKFrequent(nums: IntArray, k: Int): IntArray {
            val map = HashMap<Int, Int>()
            val list = ArrayList<Int>()
            val arr = IntArray(k)
            for (n in nums) {
                map[n] = map.getOrDefault(n, 0) + 1
            }
            for (key in map.keys) {
                var max = 0
                max = Math.max(key, max)
                if (key <= max) {
                    list.add(key)
                }
            }
           // list.sort()
            for (i in 0 until k) {

                arr[i] = list[i]
            }

            return arr
        }
    }
    companion object {
        @JvmStatic
        fun main(agrs: Array<String>) {
            val num = intArrayOf(3,3,3,3,4,1,1,1,1,1,1,1,2)//(4,1,-1,2,-1,2,3)//(1,1,1,2,2,3)
            //output 1,2
            println("num = ${num.contentToString()}")
            val res = Solution().topKFrequent(num, 2)
            println("res = ${res.contentToString()}")
        }
    }
}