class LongestConsecutiveSequence {
    //https://leetcode.com/problems/longest-consecutive-sequence/
    class Solution {
        fun longestConsecutive(nums: IntArray): Int {
            if (nums.isEmpty()) {
                return 0
            }
            nums.sort()
            var cnt = 1
            var max = 1
            for (i in 1 until nums.size) {
                if (nums[i] - nums[i-1] == 1) {
                    cnt++
                    if (cnt > max) max = cnt
                } else if (nums[i] == nums[i-1]) {
                    continue
                } else {
                    cnt = 1
                }
//                } else if (nums[i] - nums[i-1] != 0) {//nums[i] - nums[i-1] > 1 ||
//                    max = 1
                //nums[i] - nums[i-1] < 1
//                }
            }
            return max
        }
    }
    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            val nums: IntArray = intArrayOf(9,1,4,7,3,-1,0,5,8,-1,6)//intArrayOf(1,2,1,4,3)//intArrayOf(100,4,200,1,3,2)//

            val res = Solution().longestConsecutive(nums)
            println("nums after sort = ${nums.contentToString()}")
            println("res = $res")
        }
    }
}