//https://leetcode.com/problems/sort-colors/
//75 in leetcode
class SortColors {
    class Solution {
        fun sortColors(nums: IntArray): Unit {
            var k = true
            while (k) {
                k = false
                for (i in 1 until nums.size) {
                    if (nums[i] > nums[i-1]) {
                        val tmp = nums[i]
                        nums[i] = nums[i-1]
                        nums[i-1] = tmp
                        k = true
                    }
                }
            }

            nums.reverse()
        }
    }
    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            val nums: IntArray = intArrayOf(0,1,2)//intArrayOf(2,0,2,1,1,0)//

            val res = Solution().sortColors(nums)
            println("res = ${nums.contentToString()}")
        }
    }
}