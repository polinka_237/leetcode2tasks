import ForTasks.ListNode
//https://leetcode.com/problems/sort-list/description/
class SortList {
    class Solution {
        fun sortList(head: ListNode?): ListNode? {
            if (head?.next == null) return head
            var k = false
            var first : ListNode = head
            while (true) {
                while (first != null && first.next != null) {
                    val next: ListNode = first.next!!
                    if (first.value > next.value) {
                        val cnt = first.value
                        first.value = next.value
                        next.value = cnt
                        k=true
                    } else {
                        first = first.next!!
                    }
                }

                if (!k) {
                    break
                } else {
                    k = false
                    first = head
                }
            }
            return head
        }

    }
    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            var node = ListNode(-1)
            node.next = ListNode(-8)
//            node.next = ListNode(5)
//            node.next?.next = ListNode(3)
//            node.next?.next?.next = ListNode(4)
//            node.next?.next?.next?.next = ListNode(0)
            println("$node")
            val res = Solution().sortList(node)
            println("res = $res}")
        }
    }
}