class MaxPointsOnALine {
    //https://leetcode.com/problems/max-points-on-a-line/
    //Given an array of points where points[i] = [xi, yi] represents
    //a point on the X-Y plane, return the maximum number of points that lie on the same straight line.
    class Solution {
        fun maxPoints(points: Array<IntArray>): Int {
            var cnt = 0

            if (points.size <=2) {
                return points.size
            }
            for (i in 0 until points.size) {
                var map = HashMap<Double, Int>()
                for (j in i+1 until points.size) {
                    var k = ((points[j][1] - points[i][1])).toDouble()/((points[j][0] - points[i][0])).toDouble()
                    if (k < 0 || k == Double.NEGATIVE_INFINITY) {
                        k = Math.abs(k)
                    }
                    var b = (points[i][1] - k*points[i][0]).toDouble()

                }
            }

            return cnt
        }

        fun allPairs(points: Array<IntArray>): Pair<IntArray, IntArray> {
            var pair : Pair<IntArray, IntArray>? = null

            for (i in 0 until points.size-1) {
                for (j in i+1 until points.size) {
                    if (points.size <= 2) return Pair(points[i], points[j])
                    pair = allPairs(arrayOf(points[i],points[j]))
                }

            }
            return pair!!
//            return if (points.size >= 2) Pair(points[0], points[1])  else//as Pair<IntArray, IntArray>
//                arrayOf(pair?.first, pair?.second).let { allPairs(it!!) }

        }
    }
    companion object {
        @JvmStatic
        fun main(agrs: Array<String>) {
            val points = arrayOf(intArrayOf(1,1), intArrayOf(2,2), intArrayOf(3,3))

        for (i in points.indices) {
            println("Point = ${points[i].contentToString()}")
        }
        val res = Solution().allPairs(points)
            for (i in res.first.indices) {
                println("res = ${res.first[i]} res ${res.second[i]}")
            }
            println("res = $res")
        }
    }
}