import jdk.jfr.Unsigned

//190 in leetcode
//Reverse bits of a given 32 bits unsigned integer.
//https://leetcode.com/problems/reverse-bits/
class ReverseBits {
    class Solution {
        // you need treat n as an unsigned value
        fun reverseBits(n:Int):Int {
            var ret = 0
            var i = 31
//            while (i >= 0) {
//                // or: побитовая операция OR (логическое сложение или дизъюнкция)
//                // shr: сдвиг битов числа со знаком вправо
//                // shl: сдвиг битов числа со знаком влево
//                // The shl function shifts bit pattern to the left by certain number of specified bits,
//                // and zero bits are shifted into the low-order positions.
//                x = (x or ((n shr i) and (1 shl 31-i)))
//                i--
//            }
//            return x
            while (i >= 0) {
                // or: побитовая операция OR (логическое сложение или дизъюнкция)
                // shr: сдвиг битов числа со знаком вправо
                // shl: сдвиг битов числа со знаком влево
                // The shl function shifts bit pattern to the left by certain number of specified bits,
                // and zero bits are shifted into the low-order positions.
//x = (x or ((n shr i) and (1 shl 31-i)))
                ret += (((n and (1 shl i)) shr i) shl (31-i))
                i--
            }
            return ret

        }
    }
    companion object {
        @JvmStatic
        fun main (args: Array<String>) {
            val n = 0b1111111111111111111111111111101//0b0000010100101000001111010011100
            val res = Solution().reverseBits(n)
            println("res = $res")
        }
    }
}