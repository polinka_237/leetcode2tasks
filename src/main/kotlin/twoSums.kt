import com.sun.jdi.IntegerValue

class twoSums {
    class Solution {
        fun twoSum(nums: IntArray, target: Int): IntArray {
            for (i in 0..nums.size-1) {
                for (j in i+1 until nums.size-1) {
                    val sum = nums[i] + nums[j]
                    if (sum == target)
                        return intArrayOf(i,j)
                }

            }
            return intArrayOf()
        }
    }
    companion object {
        @JvmStatic
        fun main (args: Array<String>) {
            var nums = intArrayOf(2,7,11,15)
            var res = Solution().twoSum(nums, 9)
            println("res = ${res.contentToString()}")
        }
    }
}