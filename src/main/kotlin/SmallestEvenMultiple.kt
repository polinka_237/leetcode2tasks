//https://leetcode.com/problems/find-three-consecutive-integers-that-sum-to-a-given-number/
class SmallestEvenMultiple {
    class Solution {
        fun smallestEvenMultiple(n: Int): Int {
            var res = 0
            if (n % 2 == 0 && n % n == 0) return n
            else {
                res = n*2
            }
            return res
        }
    }
}