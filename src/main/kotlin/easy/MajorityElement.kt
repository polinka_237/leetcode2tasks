package easy

//169 in leetcode
//https://leetcode.com/problems/majority-element/
//Given an array nums of size n, return the majority element.
//
//The majority element is the element that appears more than ⌊n / 2⌋ times.
// You may assume that the majority element always exists in the array.
class MajorityElement {
    class Solution {
        fun majorityElement(nums: IntArray): Int {
            var cnt = 0
            var res = 0
            for (i in 0 until nums.size) {
                nums.sort()
                if (0 == cnt) {
                    res = nums[i]
                }
                if (nums[i] == res) cnt++
                else cnt--
            }

            return res
        }
        fun majorityElement2(nums: IntArray): Int {
            var count = HashMap<Int, Int>()
            for(n in nums) {
                count[n] = count.getOrDefault(n, 0) + 1
                if (count[n]!! > nums.size/2) return n
            }
            return -1
        }
    }
    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            val arr: IntArray = intArrayOf(1, 2, 1, 2, 1)
            println("arr = ${arr.contentToString()}")
            val res = Solution()
            val result = res.majorityElement2(arr)
            println("result = $result")


        }
    }
}