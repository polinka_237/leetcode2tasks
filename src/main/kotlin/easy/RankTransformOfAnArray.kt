package easy

import java.util.*


//https://leetcode.com/problems/rank-transform-of-an-array/
class RankTransformOfAnArray {
    class Solution {
        fun arrayRankTransform(arr: IntArray): IntArray {
            val len: Int = arr.size
            val map = HashMap<Int, Int>()

            val nums = IntArray(len)

            for (i in 0 until len) {
                nums[i] = arr[i]
            }
            Arrays.sort(nums)
            var tmp = 1
            for (i in 0 until len) {
                if (!map.containsKey(nums[i])) {
                    map[nums[i]] = tmp++
                }
            }

            for (i in 0 until len) {
                arr[i] = map[arr[i]]!!
            }

            return arr
        }
    }

    companion object {
        @JvmStatic
        fun main(agrs: Array<String>) {
            val arr = intArrayOf(10,20,30,40)
            println("nums = ${arr.contentToString()}")
            val res = Solution().arrayRankTransform(arr)
            println("res = ${res.contentToString()}")
        }
    }
}