package easy

class SqrtX {
    class Solution {
        fun mySqrt(x: Int): Int {
//            var cnt = Math.sqrt(x.toDouble())
//            return cnt.toInt()
            var l = 1
            var r = x

            while (l <= r) {
                val mid: Int = (l + r) / 2
                if (x / mid === mid) {
                    return mid
                } else if (mid > x / mid) {
                    r = mid - 1
                } else {
                    l = mid + 1
                }
            }

            return r
        }
    }
    companion object{
        @JvmStatic
        fun main (args: Array<String>) {
            val res = Solution().mySqrt(6)
            println("res = $res")
        }
    }
}