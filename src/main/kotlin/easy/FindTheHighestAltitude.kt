package easy
//https://leetcode.com/problems/find-the-highest-altitude/?envType=study-plan-v2&envId=leetcode-75
class FindTheHighestAltitude {
    class Solution {
        fun largestAltitude(gain: IntArray): Int {
            var tmp = 0
            var max = 0

            for (i in 0..gain.size - 1) {
                tmp += gain[i]
                if (tmp > max) {
                    max = tmp
                }
            }
            return max
        }
    }

    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            val gain = intArrayOf(-5,1,5,0,-7)
            println("gain = ${gain.contentToString()}")
            val res = Solution().largestAltitude(gain)
            println("res = $res")
        }
    }
}