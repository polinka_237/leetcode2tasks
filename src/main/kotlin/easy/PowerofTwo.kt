package easy

class PowerofTwo {
    //Given an integer n, return true if it is a power of two. Otherwise, return false.
    //https://leetcode.com/problems/power-of-two/
    //An integer n is a power of two, if there exists an integer x such that n == 2x.
    class Solution {
        fun isPowerOfTwo(n: Int): Boolean {
            if (n == 1) return true
            if (n <= 0 || n%2 !=0 ) return false
            else return isPowerOfTwo(n/2)
        }
    }
    companion object {
        @JvmStatic
        fun main(agrs: Array<String>) {
            val res = Solution().isPowerOfTwo(16)
            println("res = $res" )
        }
    }
}