package easy

class LongestPalindrome {
    class Solution {
        fun longestPalindrome(s: String): Int {
            if (s.isEmpty()) return 0

            val map : MutableMap<Char, Int> = HashMap()
            var cnt = 0
            var flag = false
            for (i in s.toCharArray()) {
                map[i] = map.getOrDefault(i, 0) + 1

            }
            for (j in map.keys) {
                if (map[j]!! % 2 == 0) {
                   cnt += map[j]!!
                } else {
                    flag = true
                    cnt += map[j]!! - 1
                }
            }
          return if (flag) cnt + 1 else cnt

        }
    }
    companion object {
        @JvmStatic
        fun main(agrs: Array<String>) {
            val a = "adamm"
            val res = Solution().longestPalindrome(a)
            println("res = $res")
        }
    }
}