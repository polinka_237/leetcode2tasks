package easy

class RunningSum {
    class Solution {
        fun runningSum(nums: IntArray): IntArray {
            val res = IntArray(nums.size)
            var sum = 0
            for (i in 0 until nums.size) {
                sum = sum + nums[i]
                res[i] = sum
            }
            return res
        }

        fun pivotIndex(nums: IntArray): Int {
            if(nums.size == 0) return -1
            var right = 0
            var left = 0
            for (i in nums) right += i

            for (i in 0 until nums.size) {
                right -= nums[i]
                if (right === left) return i
                left += nums[i]
            }
            return -1
        }
    }
    companion object {
        @JvmStatic
        fun main(agrs: Array<String>) {
            val nums = intArrayOf(1,1,1,1,1)
            val result = Solution().runningSum(nums)
            println("${result.contentToString()}")
            val res = Solution().pivotIndex(nums)
            println("$res")
        }
    }
}