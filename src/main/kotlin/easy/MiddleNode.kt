package easy

import ForTasks.ListNode

class MiddleNode {
    class Solution {
        fun middleNode(head: ListNode?): ListNode? {
            var listCnt = head
            var listSize = 0
            while (listCnt != null) {
                listSize++
                listCnt = listCnt?.next
            }
            listCnt = head
            for (i in 0 until listSize/2) {
                listCnt = listCnt!!.next
            }

            return listCnt
        }
    }
    companion object {
        @JvmStatic
        fun main(agrs: Array<String>) {
            var node = ListNode(1)
            node.next = ListNode(2)
            node.next?.next = ListNode(3)
            node.next?.next?.next = ListNode(4)
            node.next?.next?.next?.next = ListNode(5)
            val res = Solution().middleNode(node)
            println("res = $res")
        }
    }
}