package easy

//https://leetcode.com/problems/move-zeroes/
//283 in leetcode
//Given an integer array nums,
//move all 0's to the end of it while maintaining the relative order of the non-zero elements.
//
//Note that you must do this in-place without making a copy of the array.
class MoveZeroes {
    class Solution {
        fun moveZeroes(nums: IntArray): Unit {
            var cnt = 0
            for (i in nums.indices) {
                if (nums[i] == 0) {
                    cnt++
                } else if (cnt > 0) {
                    var tmp = nums[i]
                    nums[i] = 0
                    nums[i - cnt] = tmp
                }

            }

        }
    }
    companion object{
        @JvmStatic
        fun main(args: Array<String>) {
            val nums: IntArray = intArrayOf(0,1,0,0,3,12,0,1,1,0)
            println("prices = ${nums.contentToString()}")
            val res = Solution()
            val result = res.moveZeroes(nums)
            println("result = ${nums.contentToString()}")

        }
    }
}