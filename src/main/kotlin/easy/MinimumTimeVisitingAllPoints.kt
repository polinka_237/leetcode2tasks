package easy

import java.lang.StrictMath.abs
import kotlin.math.max

class MinimumTimeVisitingAllPoints {
    class Solution {
        fun minTimeToVisitAllPoints(points: Array<IntArray>): Int {
            var res = 0
            for (i in 1 until points.size) {
                val first = points[i-1]
                val second = points[i]
                res = res + max(abs(first[0] - second[0]), abs(first[1] - second[1]))
            }
            return res
        }
    }
}