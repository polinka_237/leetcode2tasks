package easy
//https://leetcode.com/problems/find-the-difference/

class FindTheDifference {
    class Solution {
        fun findTheDifference(s: String, t: String): Char {

            val smap = mutableMapOf<Char, Int>()
            val tmap = mutableMapOf<Char, Int>()
            s.forEach {
                smap.put(it, smap.getOrDefault(it,0)+1)
            }
            t.forEach {
                tmap.put(it, tmap.getOrDefault(it,0)+1)
            }
            tmap.forEach{ (key, value) ->
                if (!smap.contains(key) || smap.get(key) != value) {
                    return key
                }
            }

return ' '
        }
    }
    companion object {
        @JvmStatic
        fun main(agrs: Array<String>) {
            val s = "abcd"
            val t = "abcde"
            val res = Solution().findTheDifference(s,t)
            println("res = $res")
        }
    }
}