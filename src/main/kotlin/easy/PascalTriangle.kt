package easy//118. Pascal's Triangle in leetcode
//Given an integer numRows, return the first numRows of Pascal's triangle.
//https://leetcode.com/problems/pascals-triangle/

class PascalTriangle {
    class Solution {
        fun generate(numRows: Int): List<List<Int>> {
            val result = ArrayList<List<Int>>()
            result.add(listOf(1))

            for (i in 2 .. numRows) {
                val cnt = result.last()
                val row = mutableListOf<Int>()
                row.add(cnt[0])
                for (j in 1..cnt.lastIndex) {
                    row.add(cnt[j - 1] + cnt[j])
                }
                row.add(cnt[0])
                result.add(row)
            }

            return result
        }
    }
    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            val numRows = 5
            val solution = Solution()
            val res = solution.generate(numRows)
            println("res = $res")
        }
    }
}