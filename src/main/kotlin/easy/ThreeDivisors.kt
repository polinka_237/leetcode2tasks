package easy
//https://leetcode.com/problems/three-divisors/
class ThreeDivisors {
    class Solution {
        fun isThree(n: Int): Boolean {
            var count = 0
            for (i in 1..n) {
                if (n % i == 0) {
                    count++
                }
                if (count > 3) {
                    return false
                }
            }
            if (count == 3) {
                return true
            }
            return false
        }
    }
    companion object {
        @JvmStatic
        fun main(agrs: Array<String>) {
            val num = 2
            println("num = $num")
            val res = Solution().isThree(num)
            println("res = $res")
        }
    }
}