package easy

class CountSquareSumTriples {
    class Solution {
        fun countTriples(n: Int): Int {
            var count = 0
            for (a in 1 until n) {
                for (b in a+1 until n) {
                    val sq = a * a + b * b
                    val c = Math.sqrt(sq.toDouble()).toInt()
                    if (sq == c*c && c <= n) count +=2
                }
            }
            return count

        }
    }
    companion object {
        @JvmStatic
        fun main(agrs: Array<String>) {
            val num = 10
            println("num = $num")
            val res = Solution().countTriples(num)
            println("res = $res")
        }
    }
}