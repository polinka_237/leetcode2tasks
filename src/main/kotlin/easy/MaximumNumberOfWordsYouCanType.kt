package easy

import java.awt.SystemColor


//https://leetcode.com/problems/maximum-number-of-words-you-can-type/
//There is a malfunctioning keyboard where some letter keys do not work. All other keys on the keyboard work properly.
//
//Given a string text of words separated by a single space (no leading or trailing spaces) and a string brokenLetters
// of all distinct letter keys that are broken, return the number of words in text you can fully type using this keyboard.
class MaximumNumberOfWordsYouCanType {
    class Solution {
        fun canBeTypedWords(text: String, brokenLetters: String): Int {
            val arr = text.split(" ")
            val size = arr.size

            if (brokenLetters == "") return size
            var ans = size

            for (t in arr) {
                for (i in 0..brokenLetters.length - 1) {
                    if (t.contains(brokenLetters[i])) {
                        ans--
                        break
                    }
                }
            }

            return ans
        }
    }

    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            val text = "hello world"
            val broken = "ad"
            println("text = $text   broken = $broken")
            val res = Solution().canBeTypedWords(text, broken)
            println("res = $res")
        }
    }
}