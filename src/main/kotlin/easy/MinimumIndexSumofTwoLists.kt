package easy
//https://leetcode.com/problems/minimum-index-sum-of-two-lists/
class MinimumIndexSumofTwoLists {
    class Solution {
        fun findRestaurant(list1: Array<String>, list2: Array<String>): Array<String> {
            val arr = ArrayList<String>()
            val map = HashMap<String, Int>()
            val dupl = HashMap<String, Int>()
            var min = Int.MAX_VALUE
            for (i in 0..list1.size-1) map.put(list1[i], i)

            for (j in 0..list2.size-1) {
                if (map.contains(list2[j])) {
                    val sum = j + map[list2[j]]!!
                    min = minOf(min, sum)
                    dupl.put(list2[j], sum)
                }
            }

            for (item in dupl) {
                if (item.value == min) arr.add(item.key)
            }

            return arr.toTypedArray()
        }
    }
    companion object {
        @JvmStatic
        fun main(agrs: Array<String>) {
            val list1 = arrayOf("Shogun","Tapioca Express","Burger King","KFC")
            val list2 = arrayOf("Piatti","The Grill at Torrey Pines","Hungry Hunter Steakhouse","Shogun")
            println("list1 = ${list1.contentToString()}")
            println("list2 = ${list2.contentToString()}")

            val res = Solution().findRestaurant(list1, list2)
            println("res = ${res.contentToString()}")
        }
    }
}