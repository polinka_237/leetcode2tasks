package easy

class BinarySearch {
    class Solution {
        fun search(nums: IntArray, target: Int): Int {
            var cnt = -1
            for (i in 0 until nums.size) {
                if (nums[i] == target) {
                    cnt = i
                }
            }
            return cnt
        }
    }
    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            val nums = intArrayOf(1,2,3,4)
            val res = Solution().search(nums,3)
            println("res = $res")
        }
    }

}