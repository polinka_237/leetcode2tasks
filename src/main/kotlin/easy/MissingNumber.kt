package easy

import java.util.*

//https://leetcode.com/problems/missing-number/submissions/921320932/
class MissingNumber {
    class Solution {
        fun missingNumber(nums: IntArray): Int {
            Arrays.sort(nums)
            for (i in 0 until nums.size) {
                if (nums[i] !== i) return i
            }
            return nums.size
        }
    }

    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            val nums: IntArray = intArrayOf(0,1,3)//intArrayOf(2,0,2,1,1,0)//

            val res = Solution().missingNumber(nums)
            println("res = $res")
        }
    }
}