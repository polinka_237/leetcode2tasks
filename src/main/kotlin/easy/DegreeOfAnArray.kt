package easy


//https://leetcode.com/problems/degree-of-an-array/
class DegreeOfAnArray {
    class Solution {
        fun findShortestSubArray(nums: IntArray): Int {
            var max = 0
            var result = 0
            val hash: MutableMap<Int, Int> = HashMap()

            val tmp: MutableMap<Int, Int> = HashMap()

            for (i in 0 until nums.size) {
                tmp.putIfAbsent(nums[i], i)
                hash[nums[i]] = hash.getOrDefault(nums[i], 0) + 1
                if (hash[nums[i]]!! > max) {
                    max = hash[nums[i]]!!

                    result = i - tmp[nums[i]]!! + 1
                } else if (hash[nums[i]] == max) {
                    result = Math.min(result, i - tmp[nums[i]]!! + 1)
                }
            }
            return result

        }
    }
    companion object {
        @JvmStatic
        fun main(agrs: Array<String>) {
            val nums = intArrayOf(1,2,2,3,1,4,2)//(1,2,2,3,1)
            println("nums = ${nums.contentToString()}")
            val res = Solution().findShortestSubArray(nums)
            println("res = $res")
        }
    }
}