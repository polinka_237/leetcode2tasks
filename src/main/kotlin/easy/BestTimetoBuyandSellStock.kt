package easy

import kotlin.math.max

//You are given an array prices where prices[i] is the price of a given stock on the ith day.
//
//You want to maximize your profit by choosing a single day to buy one stock and choosing a different day in the future to sell that stock.
//
//Return the maximum profit you can achieve from this transaction. If you cannot achieve any profit, return 0.
//121 in leetcode
//https://leetcode.com/problems/best-time-to-buy-and-sell-stock/
class BestTimetoBuyandSellStock {
    class Solution {
        fun maxProfit(prices: IntArray): Int {
            var maxProfit = 0
            if(prices.isEmpty()) {
                return 0
            }
            var low = prices[0]
            var high = prices[0]
            for (i in 1 until prices.size) {
                val price = prices[i]
                if (price < low) {
                    low = price
                    high = 0
                } else if (price >= high) {
                    high = price
                    maxProfit = max(maxProfit, high-low)
                }
            }
            return maxProfit

//            for (i in prices.indices) {
//                for (j in i+1 until prices.size) {
//                    cnt = prices[j] - prices[i]
//                    if (cnt > maxProfit) {
//                        maxProfit = cnt
//                    }
//                    else continue
//                    return maxProfit
//                }
//            }
//            return 0
        }

    }
    companion object{
        @JvmStatic
        fun main(args: Array<String>) {
            val prices: IntArray = intArrayOf(7,1,5,3,6,4)//(4, 2, 3, 5, 9)
            println("prices = ${prices.contentToString()}")
            val res = Solution()
            val result = res.maxProfit(prices)
            println("result = $result")
        }
    }
}