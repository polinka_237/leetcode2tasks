package easy

import medium.ContinuousSubarraySum

//https://leetcode.com/problems/plus-one/?envType=study-plan-v2&envId=top-interview-150
class PlusOne {
    class Solution {
        fun plusOne(digits: IntArray): IntArray {
            for (i in digits.size-1 downTo 0) {
                digits[i] = digits[i] + 1
                if (digits[i] <=9) return digits
                digits[i] = 0
            }
            val arr = IntArray(digits.size+1)
            arr[0] = 1
            return arr
        }
    }
    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            val nums = intArrayOf(4,3,2,1)
            println("nums = ${nums.contentToString()}")
            val res = Solution().plusOne(nums)
            println("res = ${res.contentToString()}")
        }
    }
}