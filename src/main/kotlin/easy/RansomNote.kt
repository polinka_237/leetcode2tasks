package easy

class RansomNote {
    //https://leetcode.com/problems/ransom-note/
    //383 in leetcode
    //Given two strings ransomNote and magazine, return true if ransomNote can be constructed by using the letters from magazine and false otherwise.
    //
    //Each letter in magazine can only be used once in ransomNote.
    class Solution {
        fun canConstruct(ransomNote: String, magazine: String): Boolean {
            if (ransomNote.length > magazine.length) return false
            val result = mutableMapOf<Char, Int>()
            for (i in ransomNote.toCharArray()) {
                result[i] = result.getOrDefault(i, 0) + 1
            }

            for (i in magazine.toCharArray()) {
                if (result.containsKey(i)) {
                    result[i] = result.getOrDefault(i, 0) - 1
                }
            }

            val cnt = 0
            for (cnt in result.values) {
                if (cnt > 0) {
                    return false
                }
            }

            return true
        }
    }
    companion object {
        @JvmStatic
        fun main(args:Array<String>) {
            val ransomNote = "bb"
            val magazine = "abb"
            val res = Solution().canConstruct(ransomNote, magazine)
            println("res = $res")
        }
    }

}