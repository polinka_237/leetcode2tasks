package easy
//https://leetcode.com/problems/check-if-the-number-is-fascinating/
class CheckIfTheNumberIsFascinating {
    class Solution {
        fun isFascinating(n: Int): Boolean {
            val numbers = intArrayOf(0, 1, 2, 3, 4, 5, 6, 7, 8, 9)
            var tmp = n
            var n2 = tmp * 2
            var n3 = tmp * 3
            if (tmp > 333) return false
            while (tmp > 0) {
                if (numbers[tmp % 10] == 0) {
                    return false
                }
                numbers[n % 10] = 0
                tmp /= 10
            }
            while (n2 > 0) {
                if (numbers[n2 % 10] == 0) {
                    return false
                }
                numbers[n2 % 10] = 0
                n2 /= 10
            }
            while (n3 > 0) {
                if (numbers[n3 % 10] == 0) {
                    return false
                }
                numbers[n3 % 10] = 0
                n3 /= 10
            }
            return true
        }
    }
    companion object {
        @JvmStatic
        fun main(agrs: Array<String>) {
            val n = 192
            val res = Solution().isFascinating(n)
            println("res = $res")

        }
    }
}