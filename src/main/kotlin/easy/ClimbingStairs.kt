package easy

class ClimbingStairs {
    class Solution {
        fun climbStairs(n: Int): Int {
            var preOne = 1
            var preTwo = 1
            for (index in 2..n) {
                val sum = preOne + preTwo
                preTwo = preOne
                preOne = sum
            }
            return preOne
        }
    }
    companion object {
        @JvmStatic
        fun main (args: Array<String>) {
            val n = 5
            val res = Solution().climbStairs(n)
            println("res = $res")
        }
    }
}