package easy


//https://leetcode.com/problems/keyboard-row/
class KeyboardRow {
    class Solution {
        fun findWords(words: Array<String>): Array<String> {
//            val row1 = hashSetOf('q', 'w', 'e', 'r', 't', 'y', 'u', 'i', 'o', 'p')
//            val row2 = hashSetOf('a', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l')
//            val row3 = hashSetOf('z', 'x', 'c', 'v', 'b', 'n', 'm')
//            var res = ArrayList<String>()
//
//
//
            val row1 = "qwertyuiopQWERTYUIOP"
            val row2 = "asdfghjklASDFGHJKL"
            val row3 = "zxcvbnmZXCVBNM"
            val resLIst = ArrayList<String>()
            val map = HashMap<Char, Int>()
            for (i in 0 until row1.length) {
                val c = row1[i]
                map[c] = 1
            }
            for (i in 0 until row2.length) {
                map[row2[i]] = 2
            }
            for (i in 0 until row3.length) {
                map[row3[i]] = 3
            }

            for (i in 0 until words.size) {
                val word = words[i]
                var c1 = 0
                var c2 = 0
                var c3 = 0
                for (j in 0 until word.length) {
                    val cnt = word[j]
                    if (map[cnt] == 1) {
                        c1++
                    } else if (map[cnt] == 2) {
                        c2++
                    } else {
                        c3++
                    }
                }
                if (c1 == word.length || c2 == word.length || c3 == word.length) {
                    resLIst.add(word)
                }
            }
            return resLIst.toTypedArray()
        }
    }
    companion object {
        @JvmStatic
        fun main(agrs: Array<String>) {
            val words = arrayOf("Hello","Alaska","Dad","Peace")
            println("nums = ${words.contentToString()}")
            val res = Solution().findWords(words)
            println("res = ${res.contentToString()}")
        }
    }
}