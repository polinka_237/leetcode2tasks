package easy

class MinimumSumofFourDigitNumberAfterSplittingDigits {
    class Solution {
        fun minimumSum(num: Int): Int {
            val arr = IntArray(4)
            var n = num
            var i = 0
            while (n > 0) {
                arr[i] = n % 10
                i++
                n /= 10
            }
            arr.sort()
            return arr[0]*10 + arr[2] + arr[1]*10 + arr[3]
        }
    }
    companion object {
        @JvmStatic
        fun main(agrs: Array<String>) {
            val num = 2932
            println("num = $num")
            val res = Solution().minimumSum(num)
            println("res = $res")
        }
    }
}