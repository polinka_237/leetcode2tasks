package easy

//136 in leetcode
//https://leetcode.com/problems/single-number/
class SingleNumber {
    class Solution {
        fun singleNumber(nums: IntArray): Int {
            var cnt = 0
            for (i in 0 until nums.size) {
                cnt = cnt xor nums[i]
            }
            return cnt
        }
    }
    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
//            val nums: IntArray = intArrayOf(0,2,3,2,0)
            val nums: IntArray = intArrayOf(255,255,325252,0,0)
            //val nums: IntArray = intArrayOf(0,2,3,2,0)

            println("prices = ${nums.contentToString()}")
            val res = Solution()
            val result = res.singleNumber(nums)
            println("result = $result")
        }
    }
}