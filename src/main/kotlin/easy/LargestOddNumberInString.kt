package easy

class LargestOddNumberInString {
    class Solution {
        fun largestOddNumber(num: String): String {
            var number = ""
            for (i in num.length-1 downTo 0) {
                if(num[i].toInt() % 2 == 1) {
                    number = num.substring(0,i+1)
                    break
                }

            }

            return number
        }
    }
    companion object {
        @JvmStatic
        fun main(agrs: Array<String>) {
            val num = "52"
            println("num = $num")
            val res = Solution().largestOddNumber(num)
            println("res = $res")
        }
    }
}