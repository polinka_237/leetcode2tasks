import kotlin.math.max

class MaxPointsOnaLine2 {
    class Solution {
        fun maxPoints(points: Array<IntArray>): Int {
            return 0
        }


        fun getPairs(arr: Array<Point>) : HashSet<PairPoints> {
            val ret = HashSet<PairPoints>()

            for (i in arr.indices) {
                for (k in i+1 until arr.size) {
                    if (i >= k) continue
                    ret.add(PairPoints(arr[i], arr[k]))
                }
            }
            return ret
        }
//нужно сделать перебор пар в обратном порядке 

        fun getPairs2(arr: Array<Point>, points: HashSet<PairPoints>? = null, startIndex: Int = -1) : HashSet<PairPoints> {
            var ret = points
            if (ret == null) {
                ret = HashSet<PairPoints>()
            }

            for (i in startIndex+1 until arr.size) {
                if (startIndex < 0) {
                    ret.addAll(getPairs2(arr, ret, i))
                } else if (startIndex < i) {
                    ret.add(PairPoints(arr[startIndex], arr[i]))
                }
            }
            return ret
        }

        fun getPairs3(arr: Array<Point>) : HashSet<PairPoints> {
            val ret = HashSet<PairPoints>()
            for (i in arr.size downTo 0) {
                for (k in i+1 until arr.size) {
                    if (i >= k) continue
                    ret.add(PairPoints(arr[i], arr[k]))
                }
            }
            return ret
        }

        fun getPairs4(arr: Array<Point>, points: HashSet<PairPoints>? = null, startIndex: Int = -1) : HashSet<PairPoints> {
            var ret = points
            if (ret == null) {
                ret = HashSet<PairPoints>()
            }

            for (i in startIndex+1 until arr.size) {
                if (startIndex > 0) {
                    ret.addAll(getPairs4(arr, ret, i))
                } else if (startIndex > i) {
                    ret.add(PairPoints(arr[startIndex], arr[i]))
                }
            }
            return ret
        }

    }


    data class Point(val x: Double, val y: Double){}
    data class PairPoints(val p1: Point, val p2: Point){}
    data class Line(val k: Double, val b: Double){}

    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            val arr = arrayOf(Point(1.0,1.0),
                Point(2.0, 2.0),
                Point(3.0, 3.0),
                Point(4.0, 2.0))

            Solution().getPairs(arr).let {
                for (i in it) {
                    println("i1 $i")
                }
            }

            println(">>>>>>>>>>>>>>>>>>>>>>>>")
            Solution().getPairs2(arr).let {
                for (i in it) {
                    println("i2 $i")
                }
            }

            println(">>>>>>>>>>>>>>>>>>>>>>>>")

            Solution().getPairs3(arr).let {
                for (i in it) {
                    println("i3 $i")
                }
            }

            println(">>>>>>>>>>>>>>>>>>>>>>>>")

            Solution().getPairs4(arr).let {
                for (i in it) {
                    println("i4 $i")
                }
            }

        }
    }
}